FROM tomcat:9.0-alpine

COPY target/sbp.war /usr/local/tomcat/webapps

CMD ["catalina.sh", "run"]