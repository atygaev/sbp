package com.example.sbp.db;

import com.example.sbp.db.context.DbContext;
import com.example.sbp.db.domain.Account;
import com.example.sbp.db.domain.User;

import java.util.List;

public class AccountDb {

    public static List<Account> fetchAll() throws Exception {
        return DbContext.executeAsList("from Account order by userId, id", Account.class);
    }

    public static List<Account> fetchAllByPhone(String phone) throws Exception {
        return DbContext.executeAsList("select" +
                "   new com.example.sbp.db.domain.Account(id, amount)" +
                " from" +
                "   Account" +
                " where" +
                "   userId in (select id from User where phone = :phone)" +
                " order by" +
                "   id", Account.class, "phone", phone);
    }

    public static void create(int userId, int amount) throws Exception {
        DbContext.executeWithoutResult(session -> {
            if (session.find(User.class, userId) != null) {
                Account account = new Account();
                account.setUserId(userId);
                account.setAmount(amount);

                session.save(account);
            }
        });
    }
}
