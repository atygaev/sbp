package com.example.sbp.db;

import com.example.sbp.db.context.DbContext;
import com.example.sbp.db.domain.Account;
import com.example.sbp.db.domain.User;

import java.util.List;

public class UserDb {

    public static void saveUser(String phone) throws Exception {
        DbContext.executeWithoutResult(session -> {
            User user = new User();
            user.setPhone(phone);

            session.save(user);
            session.flush();
        });
    }

    public static List<User> fetchAll() throws Exception {
        return DbContext.executeAsList("from User order by id", User.class);
    }

    public static List<Account> fetchUserAccounts(int userId) throws Exception {
        return DbContext.executeAsList("select" +
                "   new com.example.sbp.db.domain.Account(id, amount)" +
                " from" +
                "   Account" +
                " where" +
                "   userId = :userId" +
                " order by" +
                "   id", Account.class, "userId", userId);
    }
}
