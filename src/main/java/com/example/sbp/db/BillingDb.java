package com.example.sbp.db;

import com.example.sbp.db.context.DbContext;
import com.example.sbp.db.domain.Account;

public class BillingDb {

    public static void makePayment(int acc1, int acc2, int summa) throws Exception {
        DbContext.executeWithoutResult(session -> {
            Account account1 = session.find(Account.class, acc1);

            if (account1 == null) {
                return;
            }

            Account account2 = session.find(Account.class, acc2);

            if (account2 == null) {
                return;
            }

            account1.setAmount(account1.getAmount() - summa);

            account2.setAmount(account2.getAmount() + summa);
        });
    }
}
