package com.example.sbp.db.context;

import org.flywaydb.core.Flyway;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.postgresql.ds.PGSimpleDataSource;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.List;

public class DbContext {

    private static final ThreadLocal<SessionFactory> sessionFactories = new ThreadLocal<>();

    private static DataSource dataSource = null;

    public interface DbActionWithoutResult {
        void execute(Session session) throws Exception;
    }

    public interface DbAction<T> {
        T execute(Session session) throws Exception;
    }

    static {
        if (System.getenv("DB_URL") != null) {
            PGSimpleDataSource simpleDataSource = new PGSimpleDataSource();
            simpleDataSource.setUrl(System.getenv("DB_URL"));

            dataSource = simpleDataSource;
        } else {
            InitialContext initialContext = null;
            try {
                initialContext = new InitialContext();
            } catch (NamingException e) {
                e.printStackTrace();
            }

            try {
                if (initialContext != null) {
                    dataSource = (DataSource) initialContext.lookup("java:/comp/env/jdbc/postgres");
                }
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }

        initDb();
    }

    private static void initDb() {
        Flyway.configure()
                .dataSource(getDataSource())
                .baselineOnMigrate(true)
                .load()
                .migrate();
    }

    public static DataSource getDataSource() {
        return dataSource;
    }

    public static void set(SessionFactory sessionFactory) {
        sessionFactories.set(sessionFactory);
    }

    public static SessionFactory get() {
        return sessionFactories.get();
    }

    public static void clear() {
        sessionFactories.remove();
    }

    public static void executeWithoutResult(DbActionWithoutResult dbAction) throws Exception {
        try (Session session = get().getCurrentSession()) {
            Transaction transaction = session.beginTransaction();

            try {
                dbAction.execute(session);
                transaction.commit();
            } catch (Exception e) {
                transaction.rollback();
                throw e;
            }
        }
    }

    public static <T> T execute(DbAction<T> dbAction) throws Exception {
        try (Session session = get().getCurrentSession()) {
            Transaction transaction = session.beginTransaction();

            try {
                T result = dbAction.execute(session);
                transaction.commit();
                return result;
            } catch (Exception e) {
                transaction.rollback();
                throw e;
            }
        }
    }

    public static <T> List<T> executeAsList(String hql, Class<T> entityClass) throws Exception {
        return executeAsList(hql, entityClass, null, null);
    }

    public static <T> List<T> executeAsList(String hql, Class<T> entityClass, String paramName, Object paramValue) throws Exception {
        return execute(session -> {
            Query<T> query = session.createQuery(hql, entityClass);

            if (paramName != null) {
                query.setParameter(paramName, paramValue);
            }

            return query.getResultList();
        });
    }
}
