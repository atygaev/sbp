package com.example.sbp.service;

import com.example.sbp.db.AccountDb;
import com.example.sbp.db.domain.Account;

import java.util.Collections;
import java.util.List;

public class AccountService {

    public static List<Account> fetchAll() {
        try {
            return AccountDb.fetchAll();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    public static List<Account> fetchAllByPhone(String phone) {
        try {
            return AccountDb.fetchAllByPhone(phone);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    public static void create(int userId, int amount) {
        try {
            AccountDb.create(userId, amount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
