package com.example.sbp.service;

import com.example.sbp.db.BillingDb;

public class BillingService {

    public static void makePayment(int acc1, int acc2, int summa) {
        try {
            BillingDb.makePayment(acc1, acc2, summa);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
