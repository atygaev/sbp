package com.example.sbp.service;

import com.example.sbp.db.UserDb;
import com.example.sbp.db.domain.Account;
import com.example.sbp.db.domain.User;
import org.hibernate.exception.ConstraintViolationException;

import java.util.Collections;
import java.util.List;

public class UserService {

    public static boolean saveUser(String phone) {
        try {
            UserDb.saveUser(phone);
            return true;
        } catch (Exception e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                return false;
            }
            throw new RuntimeException(e);
        }
    }

    public static List<Account> fetchUserAccounts(int userId) {
        try {
            return UserDb.fetchUserAccounts(userId);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    public static List<User> fetchAll() {
        try {
            return UserDb.fetchAll();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }
}
