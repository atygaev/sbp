package com.example.sbp.web.context;

import com.example.sbp.db.context.DbContext;
import com.example.sbp.db.domain.Account;
import com.example.sbp.db.domain.User;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.logging.Logger;

@WebListener
public class HibernateSessionFactoryInitializerListener implements ServletContextListener {

    public static final String SESSION_FACTORY_ATTRIBUTE = "SessionFactory";

    public final Logger logger = Logger.getLogger(HibernateSessionFactoryInitializerListener.class.getName());

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        SessionFactory sessionFactory = (SessionFactory) servletContextEvent.getServletContext().getAttribute("SessionFactory");
        if ((sessionFactory != null) && !sessionFactory.isClosed()) {
            logger.info("Closing sessionFactory");
            sessionFactory.close();
        }

        logger.info("Released Hibernate sessionFactory resource");
    }

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        configuration.addAnnotatedClass(User.class);
        configuration.addAnnotatedClass(Account.class);

        logger.info("Hibernate Configuration created successfully");

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .applySetting(Environment.DATASOURCE, DbContext.getDataSource())
                .build();

        logger.info("ServiceRegistry created successfully");

        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        logger.info("SessionFactory created successfully");

        servletContextEvent.getServletContext().setAttribute(SESSION_FACTORY_ATTRIBUTE, sessionFactory);

        logger.info("Hibernate SessionFactory Configured successfully");
    }
}
