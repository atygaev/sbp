package com.example.sbp.web.context;

import com.example.sbp.db.context.DbContext;
import org.hibernate.SessionFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

import static com.example.sbp.web.context.HibernateSessionFactoryInitializerListener.SESSION_FACTORY_ATTRIBUTE;

public class HibernateSessionInitializerFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            ServletContext servletContext = request.getServletContext();

            SessionFactory sessionFactory = (SessionFactory) servletContext.getAttribute(SESSION_FACTORY_ATTRIBUTE);

            DbContext.set(sessionFactory);

            chain.doFilter(request, response);
        } finally {
            DbContext.clear();
        }
    }
}
