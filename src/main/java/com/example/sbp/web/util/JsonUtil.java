package com.example.sbp.web.util;

import com.google.gson.Gson;

import java.io.InputStream;
import java.io.InputStreamReader;

public class JsonUtil {

    private static final Gson jsonMapper = new Gson();

    public static String toJson(Object object) {
        try {
            return jsonMapper.toJson(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T fromJson(InputStream inputStream, Class<T> targetClass) {
        return jsonMapper.fromJson(new InputStreamReader(inputStream), targetClass);
    }
}
