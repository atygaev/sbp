package com.example.sbp.web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HelloWorldServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String uri = req.getRequestURI();

        String name = uri.substring(uri.indexOf("/hello/") + "/hello/".length());

        res.getWriter().println(String.format("Hello, %s", name) + ". My name is sbp.");
        res.getWriter().flush();
    }
}
