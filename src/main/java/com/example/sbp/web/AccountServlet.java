package com.example.sbp.web;

import com.example.sbp.web.api.AccountApi;
import com.example.sbp.web.api.dto.CreateAccountRequest;
import com.example.sbp.web.api.dto.FindAccountsResponse;
import com.example.sbp.web.util.JsonUtil;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AccountServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (req.getAttribute("userId") != null && req.getAttribute("amount") != null) {
            int userid = (Integer) req.getAttribute("userId");

            int amount = (Integer) req.getAttribute("amount");

            CreateAccountRequest createAccountRequest = new CreateAccountRequest();
            createAccountRequest.setUserId(userid);
            createAccountRequest.setAmount(amount);

            AccountApi.create(createAccountRequest);
            return;
        }

        CreateAccountRequest createAccountRequest = JsonUtil.fromJson(req.getInputStream(), CreateAccountRequest.class);

        AccountApi.create(createAccountRequest);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
        final FindAccountsResponse response = AccountApi.find(req.getParameter("phone"));

        res.getWriter().println(JsonUtil.toJson(response));
        res.getWriter().flush();
    }
}
