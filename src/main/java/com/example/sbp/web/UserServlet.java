package com.example.sbp.web;

import com.example.sbp.web.api.UserApi;
import com.example.sbp.web.api.dto.CreateUserRequest;
import com.example.sbp.web.util.JsonUtil;
import com.google.gson.JsonObject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserServlet extends HttpServlet {

    private static final String USER_ACCOUNTS_URI_REGEX = ".*/users/(\\d+)/accounts.*";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (req.getRequestURI().matches(USER_ACCOUNTS_URI_REGEX)) {
            JsonObject requestJson = JsonUtil.fromJson(req.getInputStream(), JsonObject.class);

            int amount = requestJson.has("amount") ? requestJson.getAsJsonPrimitive("amount").getAsInt() : 0;

            req.setAttribute("userId", extractUserId(req.getRequestURI()));
            req.setAttribute("amount", amount);

            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/accounts");
            dispatcher.forward(req, res);

            return;
        }

        CreateUserRequest request = JsonUtil.fromJson(req.getInputStream(), CreateUserRequest.class);

        int resCode = UserApi.create(request);

        res.setStatus(resCode);
        res.getWriter().flush();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
        final Object response;

        if (req.getRequestURI().matches(USER_ACCOUNTS_URI_REGEX)) {
            int userId = extractUserId(req.getRequestURI());

            response = UserApi.findUserAccounts(userId);
        } else {
            response = UserApi.findAll();
        }

        res.getWriter().println(JsonUtil.toJson(response));
        res.getWriter().flush();
    }

    private static int extractUserId(String uri) {
        return Integer.parseInt(uri.replaceAll(USER_ACCOUNTS_URI_REGEX, "$1"));
    }
}
