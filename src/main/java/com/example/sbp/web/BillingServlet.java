package com.example.sbp.web;

import com.example.sbp.web.api.BillingApi;
import com.example.sbp.web.api.dto.CreatePaymentRequest;
import com.example.sbp.web.util.JsonUtil;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BillingServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
        CreatePaymentRequest createPaymentRequest = JsonUtil.fromJson(req.getInputStream(), CreatePaymentRequest.class);

        BillingApi.create(createPaymentRequest);
    }
}
