package com.example.sbp.web.api;

import com.example.sbp.db.domain.Account;
import com.example.sbp.db.domain.User;
import com.example.sbp.service.UserService;
import com.example.sbp.web.api.dto.AccountDto;
import com.example.sbp.web.api.dto.CreateUserRequest;
import com.example.sbp.web.api.dto.FindAccountsResponse;
import com.example.sbp.web.api.dto.FindUsersResponse;
import com.example.sbp.web.api.dto.UserDto;

public class UserApi {

    public static FindUsersResponse findAll() {
        FindUsersResponse response = new FindUsersResponse();

        for (User user : UserService.fetchAll()) {
            UserDto dto = new UserDto();
            dto.setId(user.getId());
            dto.setPhone(user.getPhone());

            response.add(dto);
        }

        return response;
    }

    public static FindAccountsResponse findUserAccounts(Integer userId) {
        FindAccountsResponse response = new FindAccountsResponse();

        for (Account account : UserService.fetchUserAccounts(userId)) {
            AccountDto dto = new AccountDto();
            dto.setId(account.getId());
            dto.setAmount(account.getAmount());

            response.add(dto);
        }

        return response;
    }

    public static int create(CreateUserRequest request) {
        return UserService.saveUser(request.getPhone()) ? 200 : 409;
    }
}
