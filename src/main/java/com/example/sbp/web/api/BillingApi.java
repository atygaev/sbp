package com.example.sbp.web.api;

import com.example.sbp.service.BillingService;
import com.example.sbp.web.api.dto.CreatePaymentRequest;

public class BillingApi {

    public static void create(CreatePaymentRequest request) {
        BillingService.makePayment(request.getAcc1(), request.getAcc2(), request.getSumma());
    }
}
