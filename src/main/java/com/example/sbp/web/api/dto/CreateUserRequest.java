package com.example.sbp.web.api.dto;

public class CreateUserRequest {

    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
