package com.example.sbp.web.api.dto;

public class CreatePaymentRequest {

    private Integer acc1;

    private Integer acc2;

    private Integer summa;

    public Integer getAcc1() {
        return acc1;
    }

    public void setAcc1(Integer acc1) {
        this.acc1 = acc1;
    }

    public Integer getAcc2() {
        return acc2;
    }

    public void setAcc2(Integer acc2) {
        this.acc2 = acc2;
    }

    public Integer getSumma() {
        return summa;
    }

    public void setSumma(Integer summa) {
        this.summa = summa;
    }
}
