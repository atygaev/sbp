package com.example.sbp.web.api;

import com.example.sbp.db.domain.Account;
import com.example.sbp.service.AccountService;
import com.example.sbp.web.api.dto.AccountDto;
import com.example.sbp.web.api.dto.CreateAccountRequest;
import com.example.sbp.web.api.dto.FindAccountsResponse;

import java.util.List;

public class AccountApi {

    /**
     * Returns accounts by phone if phone is specified or all accounts otherwise.
     *
     * @param phone user phone
     */
    public static FindAccountsResponse find(String phone) {
        List<Account> accounts = phone != null ? AccountService.fetchAllByPhone(phone) : AccountService.fetchAll();

        FindAccountsResponse response = new FindAccountsResponse();

        for (Account account : accounts) {
            AccountDto dto = new AccountDto();
            dto.setId(account.getId());
            dto.setAmount(account.getAmount());

            if (phone == null) {
                dto.setUserId(account.getUserId());
            }

            response.add(dto);
        }

        return response;
    }

    public static void create(CreateAccountRequest request) {
        AccountService.create(request.getUserId(), request.getAmount());
    }
}
