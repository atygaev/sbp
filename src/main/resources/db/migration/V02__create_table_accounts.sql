create table if not exists accounts
(
    id      serial  not null primary key,
    user_id integer not null,
    amount  integer not null,
    CONSTRAINT fk_user
        FOREIGN KEY (user_id)
            REFERENCES users (id)
)