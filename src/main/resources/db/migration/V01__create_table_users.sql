create table if not exists users
(
    id    serial not null primary key,
    phone text   not null unique
)