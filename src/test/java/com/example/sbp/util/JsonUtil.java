package com.example.sbp.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtil {

    private static final ObjectMapper jsonMapper = new ObjectMapper();

    public static String toJson(Object object) {
        try {
            return jsonMapper.writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static JsonNode fromJson(String jsonAsString) {
        return fromJson(jsonAsString, JsonNode.class);
    }

    public static <T> T fromJson(String jsonAsString, Class<T> targetClass) {
        try {
            return jsonMapper.readValue(jsonAsString, targetClass);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
