package com.example.sbp;

import com.example.sbp.util.JsonUtil;
import com.example.sbp.web.api.dto.AccountDto;
import com.example.sbp.web.api.dto.CreateAccountRequest;
import com.example.sbp.web.api.dto.CreatePaymentRequest;
import com.example.sbp.web.api.dto.CreateUserRequest;
import com.example.sbp.web.api.dto.FindAccountsResponse;
import com.example.sbp.web.api.dto.FindUsersResponse;
import com.example.sbp.web.api.dto.UserDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.utility.DockerImageName;

import java.util.Objects;
import java.util.Random;
import java.util.UUID;

@SpringBootTest
public class SbpApplicationTest {

    private static final PostgreSQLContainer<?> dbContainer;

    private static final GenericContainer<?> appContainer;

    static {
        Network network = Network.newNetwork();

        dbContainer = new PostgreSQLContainer<>(DockerImageName.parse("postgres"))
                .withDatabaseName("postgres")
                .withUsername("postgres")
                .withPassword("postgres")
                .withNetwork(network)
                .withNetworkAliases("db")
                .withExposedPorts(5432)
                .waitingFor(Wait.defaultWaitStrategy());

        appContainer = new GenericContainer<>(DockerImageName.parse("sbp-app"))
                .withNetwork(network)
                .withNetworkAliases("app")
                .withEnv("DB_URL", "jdbc:postgresql://db:5432/postgres?user=postgres&password=postgres")
                .withExposedPorts(8080)
                .waitingFor(Wait.forHttp("/sbp/users"))
                .dependsOn(dbContainer);

        dbContainer.start();

        appContainer.start();
    }

    public static final String BASE_URI = "http://localhost:" + appContainer.getFirstMappedPort() + "/sbp";

    private static final String USERS_API = "/users";

    private static final String ACCOUNTS_API = "/accounts";

    private static final RestTemplate httpClient = new RestTemplateBuilder()
            .rootUri(BASE_URI)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();

    @Test
    public void createUser() {
        String phone = UUID.randomUUID().toString();

        createUser(phone);
    }

    @Test
    public void createUserWithSamePhoneShouldReturn409() {
        String phone = UUID.randomUUID().toString();

        createUser(phone);

        Assertions.assertThrows(HttpClientErrorException.Conflict.class, () -> createUser(phone));
    }

    @Test
    public void createAccount() {
        String phone = UUID.randomUUID().toString();

        UserDto createdUser = createUser(phone);

        int amount = new Random().nextInt(1000);

        CreateAccountRequest createAccountRequest = new CreateAccountRequest();
        createAccountRequest.setUserId(createdUser.getId());
        createAccountRequest.setAmount(amount);

        // create new user account
        executeHttpPost(ACCOUNTS_API, createAccountRequest);

        FindAccountsResponse accounts = executeHttpGet(ACCOUNTS_API, FindAccountsResponse.class);

        for (int i = 0; i < accounts.size(); i++) {
            AccountDto account = accounts.get(i);

            if (Objects.equals(createdUser.getId(), account.getUserId()) && Objects.equals(amount, account.getAmount())) {
                return;
            }
        }

        Assertions.fail(String.format("No created user account for user = %s", createdUser.getId()));
    }

    @Test
    public void createUserAccount() {
        String phone = UUID.randomUUID().toString();

        UserDto createdUser = createUser(phone);

        int amount = new Random().nextInt(1000);

        createUserAccount(createdUser.getId(), amount);

        FindAccountsResponse accounts = executeHttpGet(ACCOUNTS_API, FindAccountsResponse.class);

        for (int i = 0; i < accounts.size(); i++) {
            AccountDto account = accounts.get(i);

            if (Objects.equals(createdUser.getId(), account.getUserId()) && Objects.equals(amount, account.getAmount())) {
                return;
            }
        }

        Assertions.fail(String.format("No created user account for user = %s and amount = %s", createdUser.getId(), amount));
    }

    @Test
    public void findUserAccountsByPhone() {
        String phone1 = "phone1." + UUID.randomUUID();
        String phone2 = "phone2." + UUID.randomUUID();

        UserDto user1 = createUser(phone1);
        UserDto user2 = createUser(phone2);

        createUserAccount(user1.getId(), 100);

        createUserAccount(user2.getId(), 111);
        createUserAccount(user2.getId(), 222);

        FindAccountsResponse user1Accounts = executeHttpGet("/accounts?phone=" + phone1, FindAccountsResponse.class);
        FindAccountsResponse user2Accounts = executeHttpGet("/accounts?phone=" + phone2, FindAccountsResponse.class);

        Assertions.assertEquals(1, user1Accounts.size());
        Assertions.assertEquals(2, user2Accounts.size());

        for (AccountDto user1Account : user1Accounts) {
            Assertions.assertEquals(100, user1Account.getAmount());
        }

        int accountNum = 1;

        for (AccountDto user2Account : user2Accounts) {
            Assertions.assertEquals((accountNum++) * 111, user2Account.getAmount());
        }
    }

    @Test
    public void makePayment() {
        UserDto user1 = createUser("payment-phone-1");
        UserDto user2 = createUser("payment-phone-2");

        AccountDto account1 = createUserAccount(user1.getId(), 100);
        AccountDto account2 = createUserAccount(user2.getId(), 100);

        CreatePaymentRequest request = new CreatePaymentRequest();
        request.setAcc1(account1.getId());
        request.setAcc2(account2.getId());
        request.setSumma(100);

        executeHttpPost("/billings", request);

        FindAccountsResponse refreshedUser1Accounts = executeHttpGet("/users/" + user1.getId() + "/accounts", FindAccountsResponse.class);
        FindAccountsResponse refreshedUser2Accounts = executeHttpGet("/users/" + user2.getId() + "/accounts", FindAccountsResponse.class);

        Assertions.assertEquals(0, refreshedUser1Accounts.get(0).getAmount());
        Assertions.assertEquals(200, refreshedUser2Accounts.get(0).getAmount());
    }

    private static UserDto createUser(String phone) {
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setPhone(phone);

        // create new user with given phone
        executeHttpPost(USERS_API, createUserRequest);

        // get all users
        FindUsersResponse users = executeHttpGet(USERS_API, FindUsersResponse.class);

        UserDto createdUser = users.stream()
                .filter(user -> phone.equals(user.getPhone()))
                .findFirst()
                .orElse(null);

        Assertions.assertNotNull(createdUser);

        return createdUser;
    }

    private static AccountDto createUserAccount(Integer userId, Integer amount) {
        CreateAccountRequest createAccountRequest = new CreateAccountRequest();
        createAccountRequest.setUserId(userId);
        createAccountRequest.setAmount(amount);

        // create new user account
        executeHttpPost(USERS_API + "/" + userId + "/accounts", createAccountRequest);

        FindAccountsResponse accounts = executeHttpGet(ACCOUNTS_API, FindAccountsResponse.class);

        AccountDto createdAccount = accounts.stream()
                .filter(account -> Objects.equals(userId, account.getUserId()))
                .findFirst()
                .orElse(null);

        Assertions.assertNotNull(createdAccount);

        return createdAccount;
    }

    private static <T> T executeHttpGet(String uri, Class<T> responseClass) {
        String rawResponse = httpClient.getForObject(uri, String.class);

        return JsonUtil.fromJson(rawResponse, responseClass);
    }

    private static <T> void executeHttpPost(String uri, T body) {
        httpClient.postForObject(uri, JsonUtil.toJson(body), String.class);
    }
}
